package com.example.demo.util;

/**
 * <h4>功能:【AJAX的方式提交表单时的响应结果。】【创建人：QiaoLiang】</h4>
 *
 * @时间:2018/6/4 0004 15:11
 * @备注:<h4></h4>
 */
public class AjaxPostResponse {

    /**
     * 成功状态。
     */
    public static final String SUCCESS = "success";

    /**
     * 失败状态，发生错误。
     */
    public static final String FAILURE = "failure";

    /**
     * 处理的状态。
     */
    private String status;

    /**
     * 发生错误时的提示信息。
     */
    private String errorMsg;

    /**
     * 成功处理后的返回结果。
     */
    private Object result;

    /**
     * Non argument constructor.
     */
    public AjaxPostResponse() {
    }

    /**
     * 成功处理后响应。
     * @param result 返回结果
     */
    public AjaxPostResponse(Object result) {
        setSuccessStatus(); // 把状态设置为“成功”
        setResult(result); // 设置响应结果
    }

    /**
     * 创建发生错误的响应。
     * @param result 发生错误时的信息实体类
     */
    public static AjaxPostResponse failure(Object result) {
        AjaxPostResponse resp = new AjaxPostResponse();
        resp.setFailureStatus();
        resp.setResult(result);
        resp.setErrorMsg("An error occurred");
        return resp;
    }

    /**
     * 创建发生错误的响应。
     * @param errorMsg 发生错误时的提示信息
     */
    public static AjaxPostResponse fail(String errorMsg) {
        AjaxPostResponse resp = new AjaxPostResponse();
        resp.setFailureStatus();
        resp.setErrorMsg(errorMsg);
        return resp;
    }

    /**
     * @return 处理状态
     * @see #SUCCESS
     * @see #FAILURE
     */
    public String getStatus() {
        return status;
    }

    /**
     * 把状态设置为“处理成功”。
     */
    public void setSuccessStatus() {
        this.status = SUCCESS;
    }

    /**
     * 把状态设置为“发生错误”。
     */
    public void setFailureStatus() {
        this.status = FAILURE;
    }

    /**
     * @return 错误提示信息
     */
    public String getErrorMsg() {
        return (null == errorMsg) ? "" : errorMsg;
    }

    /**
     * 设置错误提示信息。
     * @param errorMsg 错误提示信息
     */
    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    /**
     * @return 正常处理后的返回结果
     */
    public Object getResult() {
        return result;
    }

    /**
     * 设置正常处理后的返回结果。
     * @param result 正常返回结果
     */
    public void setResult(Object result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "AjaxPostResponse {status=" + status + ", errorMsg=" + getErrorMsg() + ", result=" + getResult() + "}";
    }

}