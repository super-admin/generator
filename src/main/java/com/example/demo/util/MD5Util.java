package com.example.demo.util;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * <h4>功能:【MD5 加密】【创建人：QIAOLIANG】</h4>
 *
 * @时间:2018/6/6 0006 11:10
 * @备注:<h4></h4>
 */
public class MD5Util {
    /**
     * MD5 加密
     *
     * @param str
     * @return
     * @throws Exception
     */
    public static String getMD5(String str) {

        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(str.getBytes("UTF-8"));
        } catch (Exception e) {
        }

        byte[] byteArray = messageDigest.digest();
        StringBuffer md5StrBuff = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            if (Integer.toHexString(0xFF & byteArray[i]).length() == 1)
                md5StrBuff.append("0").append(Integer.toHexString(0xFF & byteArray[i]));
            else
                md5StrBuff.append(Integer.toHexString(0xFF & byteArray[i]));
        }
        return md5StrBuff.toString();
    }

    /**
     * 获取随机的数值。
     *
     * @param length
     *            长度
     * @return
     */
    public static String getRandom(Integer length) {

        String result = "";
        Random rand = new Random();
        int n = 20;
        if (null != length && length > 0) {
            n = length;
        }
        boolean[] bool = new boolean[n];
        int randInt = 0;
        for (int i = 0; i < length; i++) {
            do {
                randInt = rand.nextInt(n);

            } while (bool[randInt]);

            bool[randInt] = true;
            result += randInt;
        }
        return result;
    }

    public static List<String> removeDuplicate(List<String> jurisdictionList) {

        List<String> listTemp = new ArrayList<String>();
        for (int i = 0; i < jurisdictionList.size(); i++) {
            if (!listTemp.contains(jurisdictionList.get(i))) {
                listTemp.add(jurisdictionList.get(i));
            }
        }
        return listTemp;
    }
}