package com.example.demo.util;

import javax.servlet.http.HttpServletRequest;

/**
 * <h4>功能:【判断是否为ajax请求工具类】【创建人：QIAOLIANG】</h4>
 *
 * @时间:2018/6/8 0008 14:03
 * @备注:<h4></h4>
 */
public class WebUtilsPro {


    /**
     * 是否是Ajax请求
     *
     * @param request
     * @return
     * @author SHANHY
     * @create 2017年4月4日
     */
    public static boolean isAjaxRequest(HttpServletRequest request) {
        String requestedWith = request.getHeader("x-requested-with");
        if (requestedWith != null && requestedWith.equalsIgnoreCase("XMLHttpRequest")) {
            return true;
        } else {
            return false;
        }
    }

}