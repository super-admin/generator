package com.example.demo.util;

import java.util.HashMap;
import java.util.Map;

/**
 * <h4>功能:【通用Map数据】【创建人：QIAOLIANG】</h4>
 *
 * @时间:2018/6/11 0011 9:25
 * @备注:<h4></h4>
 */
public class CommonMap {

    /** 状态编码转换 */
    public static Map<String, String> javaTypeMap = new HashMap<String, String>();

    static
    {
        initJavaTypeMap();
    }

    /**
     * 返回状态映射
     */
    public static void initJavaTypeMap()
    {
        javaTypeMap.put("TINYINT", "Integer");
        javaTypeMap.put("SMALLINT", "Integer");
        javaTypeMap.put("MEDIUMINT", "Integer");
        javaTypeMap.put("INT", "Integer");
        javaTypeMap.put("INTERGER", "Integer");
        javaTypeMap.put("BIGINT", "Long");
        javaTypeMap.put("FLOAT", "Float");
        javaTypeMap.put("DOUBLE", "Double");
        javaTypeMap.put("decimal", "BigDecimal");
        javaTypeMap.put("BIT", "Boolean");
        javaTypeMap.put("CHAR", "String");
        javaTypeMap.put("VARCHAR", "String");
        javaTypeMap.put("TINYTEXT", "String");
        javaTypeMap.put("TEXT", "String");
        javaTypeMap.put("mediumtext", "String");
        javaTypeMap.put("LONGTEXT", "String");
        javaTypeMap.put("DATE", "Date");
        javaTypeMap.put("DATETIME", "Date");
        javaTypeMap.put("TIMESTAMP", "Date");
    }

}