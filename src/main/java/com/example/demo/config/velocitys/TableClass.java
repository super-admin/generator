package com.example.demo.config.velocitys;

import java.io.File;

/**
 * <h4>功能:【数据库表字段的实体类】【创建人：QIAOLIANG】</h4>
 *
 * @时间:2018/6/27 0027 10:32
 * @备注:<h4></h4>
 */
public class TableClass {

    /** 字段名称 */
    private String fieldName;

    /** 字段类型 */
    private String fieldType;

    /** 列描述 */
    private String fieldComment;

    /** Java属性类型 */
    private String attrType;

    /** Java属性名称(第一个字母大写)，如：user_name => UserName */
    private String attrName;

    /** Java属性名称(第一个字母小写)，如：user_name => userName */
    private String attrname;


    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public void setFieldComment(String fieldComment) {
        this.fieldComment = fieldComment;
    }

    public void setAttrType(String attrType) {
        this.attrType = attrType;
    }

    public void setAttrName(String attrName) {
        this.attrName = attrName;
    }

    public void setAttrname(String attrname) {
        this.attrname = attrname;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public String getFieldComment() {
        return fieldComment;
    }

    public String getAttrType() {
        return attrType;
    }

    public String getAttrName() {
        return attrName;
    }

    public String getAttrname() {
        return attrname;
    }

}